
FabricPy
==========


FabricPy is a Python package built using Numpy and Scipy for the 
analysis of the texture and fabric of 3D images. 
The analysis is based on the autocorrelation of the image, revealing any preferential
directions and overall patterns.
The analysis can be performed at the image level, or at a local level.



Changelog
==========

| Version         | Date        | Notes                  |
|-----------------|-------------|------------------------|
| Version 0.1.3   | 2024-02-29  | Adding the function to compute the autocorrelation for different subsets of the image. |
| Version 0.1.2   | 2024-02-29  | Compatibility for python 3.8.10 - ESRF Cluster           |
| Version 0.1.1   | 2024-02-28  | First version of fabricPy, with bulk and local examples  |
