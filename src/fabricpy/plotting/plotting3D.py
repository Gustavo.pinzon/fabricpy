import numpy
import fabricpy.autocorrelation
import matplotlib.pyplot as plt

def plotACF(ACF, 
            fileName= None,
            levels = [0.2, 0.4, 0.6, 0.8],
            zoomRange = 10,
            color = 'b',
            alphaMin = 0.05,
            alphaMax = 0.5,
            show = False,
            elevation= 30,
            azimuth = 45,
            upscale = 1):
    """
    Plot surfaces of the ACF at different levels.

    Parameters
    -----------
        ACF : 3D array
            A greyscale image with the autocorrelation values for a displacement along each axes.

        fileName: str, optional
            File name with path for saving the figure. 

        levels : list, optional
            List with the different levels of the ACF to be plotted.
            Default = [0.2, 0.4, 0.6, 0.8]

        zoomRange : scalar, optional
            Limits of the different axes of the 3D plot.
            Default = 10
        
        color : str, optional
            Color of the surfaces.
            Default = 'b' (blue)

        alphaMin, alphaMax : float, optional
            Values of the transparency of the largest and smallest ellipsoids, respectively.
            Default = 0.05 and 0.5, respectively.

        show : bool, optional
            Do you want to see the plot in 3D?
            Default = False.
        
        elevation : float, optional
            Elevation of the camera for the 3D view in degrees.
            Default = 30.

        azimuth : float, optional
            Azimuth of the camera for the 3D view in degrees.
            Default = 45
            
        upscale : scalar int
            Value to upscale the slice to have sub-pixel resolution of the coordinates of each iso-surface
        
        
        
    """

    # Compute the range of alpha
    alphaRange = numpy.linspace(alphaMin, alphaMax, num = len(levels))

    # 1. Compute the convex hull at three levels
    isoSurfaces = []
    for i, lev in enumerate(levels):
        isoSurfaces.append(fabricpy.autocorrelation.computeIsoSurface(ACF, lev, upscale))
    
    # 2. Create the figure
        
    def plotConvexHull(hull, ax, color = 'b', alpha = 1):
        # Loop through each face
        for face in hull.simplices:
            points = hull.points[face]
            surf = ax.plot_trisurf(points[:,0], points[:,1], points[:,2], antialiased=True, color = color, alpha = alpha)

    fig = plt.figure(1, figsize=(4,4))
    ax1 = fig.add_subplot(1,1,1, projection='3d')
    for i,l in enumerate(levels):
        plotConvexHull(isoSurfaces[i], ax1, color = color, alpha = alphaRange[i])
    ax1.set_xlim(-zoomRange, zoomRange)
    ax1.set_ylim(-zoomRange, zoomRange)
    ax1.set_zlim(-zoomRange, zoomRange)
    ax1.set_aspect('equal')
    ax1.set_xlabel('X')
    ax1.set_ylabel('Y')
    ax1.set_zlabel('Z')
    ax1.view_init(elev=elevation, azim=azimuth)
    if fileName is not None:
        plt.savefig(fileName, dpi = 300)
    if show:
        plt.show()








 
