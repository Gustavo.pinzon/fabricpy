import numpy
import skimage.measure

def computeMomentOfInertia(ellipsoid):
    """
    Computes the eigenValues and eigeVectors of the given ellipsoid.
    The eigenValues are returned from higher to lower values.
    For a oblate ellipsoid (plate-like shapes), the orientation vector corresponds to the maximum eigenValue.
    For a prolate ellipsoid (rod-like shapes), the orientation vector corresponds to the minimum eigenValue.

    Parameters
    -----------
        ellipsoid : 3D array
            A bool image with the ellipsoid of a given isoLevel of the ACF

    Returns
    --------
        eigenValues : 1D array
            List with the ordered eigenValues from higher to lower.

        eigenVectors : 1D array
            List with the vectors in format ZYX, stacked horizontally.
            The vector of the maximum eigenvalue is in position [0:3], while the vector of the minimum eigenvalue is in position [6:9].
    """

    # Compute the intertia tensor
    inertiaTensor = skimage.measure.inertia_tensor(ellipsoid)
    # Compute the eigenValues and eigenVectors
    eigenValues, eigenVectors = numpy.linalg.eig(inertiaTensor)
    # Sort them
    idx = eigenValues.argsort()[::-1]
    eigenValues = eigenValues[idx]
    eigenVectors = eigenVectors[:,idx]

    return eigenValues, numpy.hstack((eigenVectors[:,0], eigenVectors[:,1], eigenVectors[:,2]))

def computeSemiAxesEllipsoid(ellipsoid):
    """
    Computes the semi-axes (large, medium, small) of the ellipsoid using the MOI.

    Parameters
    -----------
        ellipsoid : 3D array
            A bool image with the ellipsoid of a given isoLevel of the ACF

    Returns
    --------
        large, medium, small : scalars float
            Values of the large, medium and small semi-axes of the ellipsoid.
    
    Note
    --------
        Function based `spam.label.ellipseAxes()` of the open-source software `spam`. 

    """
    # Compute the semi axes
    # Get the volume of the ellipsoid
    vol = numpy.sum(ellipsoid>0)
    # Compute the MOI eigval and eigvect
    eigVals, _ = computeMomentOfInertia(ellipsoid)
    Ia = eigVals[0]
    Ib = eigVals[1]
    Ic = eigVals[2]
    c = ((15.0 / (8.0 * numpy.pi)) * numpy.square(Ib + Ic - Ia) / numpy.sqrt((Ia - Ib + Ic) * (Ia + Ib - Ic))) ** (1.0 / 5.0)
    b = ((15.0 / (8.0 * numpy.pi)) * numpy.square(Ic + Ia - Ib) / numpy.sqrt((Ib - Ic + Ia) * (Ib + Ic - Ia))) ** (1.0 / 5.0)
    a = ((15.0 / (8.0 * numpy.pi)) * numpy.square(Ia + Ib - Ic) / numpy.sqrt((Ic - Ia + Ib) * (Ic + Ia - Ib))) ** (1.0 / 5.0)
    volAx = (4/3)*numpy.pi*a*b*c
    volRatio = (vol/volAx)**(1./3.)
    # Update the semi-axes
    a *= volRatio
    b *= volRatio
    c *= volRatio
    # Organize them
    small, medium, large = sorted((a,b,c))

    return large, medium, small

def computeScalarAnisotropy(ellipsoid):
    """
    Computes the scalar anisotropy of an ellipsoid shape based on the eigenvalues of its inertia tensor.
    The scalar value ranges from 0 (an sphere) to 1 (a line or a plane).
    Based on 'Kinematic Model of Transient Shape-Induced Anisotropy in Dense Granular Flow. Nadler, Guillard and Einav. 2018'

    Parameters
    -----------
        ellipsoid : 3D array
            A bool image with the ellipsoid of a given isoLevel of the ACF

    Returns
    --------
        zeta : scalar, float
            Scalar anisotropy within the range [0,1]
    
    Note
    --------
        Function based `spam.label.ellipseAxes()` of the open-source software `spam`. 

    """

    eigVals, _ = computeMomentOfInertia(ellipsoid)
    eigVals /= numpy.max(eigVals)
    zeta = numpy.sqrt(0.5*((eigVals[0] - eigVals[1])**2 + (eigVals[0] - eigVals[2])**2 + (eigVals[2] - eigVals[1])**2 ))

    return zeta

def computeZinggCoordinates(ellipsoid):
    """
    Computes the coordinates of the Zingg diagram of an ellipsoid.
    The coordinates are the relation between the large and medium semi-axes (L/M) and the medium and small (M/S) semi-axes.
    
    Parameters
    -----------
        ellipsoid : 3D array
            A bool image with the ellipsoid of a given isoLevel of the ACF

    Returns
    --------
        SM, ML : scalar, float
            Values of the S/M and M/L ratio.
    """
    # Compute the axes of the ellipsoids
    L,M,S = computeSemiAxesEllipsoid(ellipsoid)
    # Compute the coordinates of the Zingg Diagram
    SM = S/M
    ML = M/L
    
    return SM, ML

def computeOrientation(ellipsoid, rawVector = False):
    """
    Computes the orientation of an ellipsoid and its inclination relative to the XY plane, using its Zingg coordinates.
    If the shape is not a lentil or a rice, we can not define an orientation vector.
    A vector that lies within the XY plane has a inclination of 0.
    A vector that is perpendicular to the XY plane has an inclination of 90.

    Parameters
    -----------
        ellipsoid : 3D array
            A bool image with the ellipsoid of a given isoLevel of the ACF

        rawVector : bool
            Do you want to retrieve the raw characteristic vectors for both shapes? (Prolate and Oblate). 

    Returns
    --------
        inclination: scalar, float
            Angle with the XY plane in degrees.
            If rawVector is False, only the inclination of the characteristic vector is returned.
            If rawVector is True, the inclination of the two characteristic vectors are returned. 
        
        vector : array, float
            Orientation vector of the ellipsoid.
            If rawVector is False, only one characteristic vector is returned.
            If rawVector is True, the two characteristic vectors are returned
    """

    # Compute the orientation of the ellipsoid
    _, eigVector = computeMomentOfInertia(ellipsoid)
    # Compute the Zingg Coord
    SM, ML = computeZinggCoordinates(ellipsoid)
    if not rawVector:
        # We only want to get a vector if we can objectively define one.

        # Check in which quadrant of the Zingg diagram we are
        if SM > 2/3 and ML < 2/3: # Rice
            vector = eigVector[6:9]
        elif SM < 2/3 and ML > 2/3: # Lentil
            vector = eigVector[0:3]
        else: 
            # If we are not a lentil or a rice, we can't define objectively a vector!
            vector = numpy.zeros((1,3))[0]
        # Flip vector if needed
        if vector[0] < 0: 
            vector *= -1
        inclination = numpy.degrees(numpy.arcsin(vector[0]))
        if numpy.linalg.norm(vector) == 0:
            # Reset the inclination if needed
            inclination = numpy.nan
        
        return inclination, vector
    else:
        # We are going to compute the two charactersitic vectors
        vectorProlate = eigVector[6:9] # Rice
        vectorOblate = eigVector[0:3] # Lentil
        inclinationProlate = numpy.degrees(numpy.arcsin(numpy.abs(vectorProlate[0])))
        inclinationOblate = numpy.degrees(numpy.arcsin(numpy.abs(vectorOblate[0]))) 

        return [inclinationProlate, inclinationOblate], [vectorProlate,vectorOblate]
    