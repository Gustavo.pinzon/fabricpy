import tifffile
import pkg_resources as pkg

def loadClay():
    dataFilePath = '/clay/clay.tif'
    if pkg.resource_exists('fabricpy', 'dataset'+dataFilePath):
        pkg.resource_filename('fabricpy','dataset'+dataFilePath)
        return tifffile.imread(pkg.resource_filename('fabricpy','dataset'+dataFilePath))
    else:
        print(IOError("File %s not found" % dataFilePath))
        return 0

def loadLentil():
    dataFilePath = '/lentil/lentil.tif'
    if pkg.resource_exists('fabricpy', 'dataset'+dataFilePath):
        pkg.resource_filename('fabricpy','dataset'+dataFilePath)
        return tifffile.imread(pkg.resource_filename('fabricpy','dataset'+dataFilePath))
    else:
        print(IOError("File %s not found" % dataFilePath))
        return 0
