import numpy
import skimage.measure
import skimage.transform
import scipy.spatial
import fabricpy.anisotropy
import matplotlib.pyplot as plt

def generateEllipsoidFromIsosurface(ACF, isoLevel, upscale = 1):
    """
    Generates the ellipsoid for a given isoLevel of the ACF using a simple threshold.

    Parameters
    -----------
        ACF : 3D array
            A greyscale image with the autocorrelation values for a displacement along each axes.

        isoLevel : scalar float
            Value of the ACF to compute its ellipsoid
            
        upscale : scalar int
            Value to upscale the slice to have sub-pixel resolution of the coordinates of each -surface

    Returns
    --------
        ellipsoid : 1D array
            A bool image with the ellipsoid of the given isoLevel
    """
    # Scale up the image if needed
    if upscale > 1:
        ACF = skimage.transform.rescale(ACF, upscale)

    ellipsoid  = ACF > isoLevel

    return ellipsoid

def computeIsoSurface(ACF, isoLevel, upscale = 1):
    """
    Computes the isosurfaces of a ACF for a given value. It uses the convex hull of the coordinates of the given ACF value. It works only for 3D for the moment.

    Parameters
    -----------
        ACF : 3D array
            A greyscale image with the autocorrelation values for a displacement along each axes.

        isoLevel : scalar float
            Value of the ACF to compute its iso-surface.
            
        upscale : scalar int
            Value to upscale the slice to have sub-pixel resolution of the coordinates of each -surface

    Returns
    --------
        hull : Hull object
            Convex hull object as returned by scipy.spatial.ConvexHull().
    """

    # Start the lists for the contour points
    pointsX = []
    pointsY = []
    pointsZ = []
    # Scale up the image if needed
    if upscale > 1:
        ACF = skimage.transform.rescale(ACF, upscale)
    # Loop over each Z level to find points of the iso
    for zLevel in range(ACF.shape[0]):
        # Get the contour at the given slice
        contour = skimage.measure.find_contours(ACF[zLevel,:,:], isoLevel)
        # Check if we got points
        if len(contour) > 0:
            xCoord = contour[0][:,0] - ACF.shape[0]//2
            yCoord = contour[0][:,1] - ACF.shape[1]//2
            zCoord = zLevel - ACF.shape[2]//2
            for i, x in enumerate(xCoord):
                pointsX.append(xCoord[i]/upscale)
                pointsY.append(yCoord[i]/upscale)
                pointsZ.append(zCoord/upscale)
        
    # Turn the points into an array
    pointsX = numpy.asarray(pointsX)
    pointsY = numpy.asarray(pointsY)
    pointsZ = numpy.asarray(pointsZ)
    # Create an array with the 3D coordinates
    coord3D = numpy.asarray([pointsX, pointsY, pointsZ]).T
    # Compute the convex hull of the points
    hull = scipy.spatial.ConvexHull(coord3D)

    return hull



def normaliseVolume(im):
    """
    Normalises a greyscale image to have a mean value of 0 and a variance of 1

    Parameters
    -----------
        im : ND array
            A greyscale array

    Returns
    --------
        imNorm : ND array
            A greyscale array with mean value = 0 and variance = 1
    """

    imNorm = (im - im.mean())/im.std()

    return imNorm

def computeAutocorrelation(im):
    """
    Computes the autocorrelation spectra of a greyscale image.

    Based on the implementation of  Thissen, C. J., & Brandon, M. T. (2015). An autocorrelation method for three-dimensional strain analysis. Journal of Structural Geology, 81, 135-154.

    Parameters
    -----------
        im : ND array
            A greyscale array

    Returns
    --------
        ACF : ND array
            A greyscale image with the autocorrelation values for a displacement along each axes.
    """

    # Normalise the image
    imNorm = normaliseVolume(im)
    # Get the number of voxels
    numElements = len(im.ravel())
    # Compute the FFT
    U = numpy.fft.fftn(imNorm)/numElements
    # Multiply it by itself
    R = numpy.multiply(U, numpy.conjugate(U))
    # Shift the zero-frequency component to the center
    ACF = numpy.real(numpy.fft.fftshift(numpy.multiply(numpy.fft.ifftn(R), numElements)))

    return ACF

def computeAutoCorrelationLength(ACF):
    """
    Computes the autocorrelation length of an ACF of a given image.
    It is defined as the maximum length of the isosurface at 50% from the center of the ACF.

    Parameters
    -----------
        ACF : 3D array
            A greyscale image with the autocorrelation values for a displacement along each axes.

    Returns
    --------
        correlationLength : scalar, float
            Length at which the ACF decreases to 0.5.
    """
    # Get the points of the 50 iso-surface
    # Get the contour at the given slice
    pointsX = []
    pointsY = []
    pointsZ = []
    # Loop over each Z level to find points of the iso
    for zLevel in range(ACF.shape[0]):
        # Get the contour at the given slice
        contour = skimage.measure.find_contours(ACF[zLevel,:,:], 0.5)
        # Check if we got points
        if len(contour) > 0:
            xCoord = contour[0][:,0] - ACF.shape[0]//2
            yCoord = contour[0][:,1] - ACF.shape[1]//2
            zCoord = zLevel - ACF.shape[2]//2
            for i, x in enumerate(xCoord):
                pointsX.append(xCoord[i])
                pointsY.append(yCoord[i])
                pointsZ.append(zCoord)
    # Turn the points into an array
    pointsX = numpy.asarray(pointsX)
    pointsY = numpy.asarray(pointsY)
    pointsZ = numpy.asarray(pointsZ)
    # Compute the distance of each point to the center of the image
    correlationLength = 0
    for i, (x,y,z) in enumerate(zip(pointsX,pointsY,pointsZ)):
        distanceTest = numpy.sqrt(z**2 + y**2 + x**2)
        if distanceTest > correlationLength:
            correlationLength = distanceTest

    return correlationLength

def computeAutoCorrelationLengthSubsets(image, nodeSpacing, plot = False):
    """
    Computes the autocorrelation length of local subsets of an image of varying sizes
    This function is used to define the proper autocorrelation length at the image scale.

    Parameters
    -----------
        image : ND array
            A greyscale array
        
        nodeSpacing : scalar, int
            Spacing between the subsets in px

        plot : bool, optional
            Should we plot the results at the end?. Default=False

    Returns
    --------
        windowSize : list, float
            List with the sizes of the windows
        
        subsetResults : 2D array, float
            Array of shape (numNodes, numSizes). Each line correspond to a different node. Each column to the size of each subset.
    """

    # Create the mesh
    nodes = makeGrid(image.shape,nodeSpacing)
    # Maximum half window size
    maxHWZ = nodes[0,0]
    # Minimum half window size
    minHWS = 1
    # List of half window sizes
    hws = numpy.arange(minHWS, maxHWZ+1)
    # Number of nodes
    numNodes = len(nodes)
    # Create the result array
    subsetResults = numpy.zeros((numNodes, len(hws)))
    # Loop over the sizes
    for i,h in enumerate(hws):
        # Loop over the nodes
        for j,n in enumerate(nodes):
            # Extract the subset
            subset = image[n[0]-h : n[0]+h,
                        n[1]-h : n[1]+h,
                        n[2]-h : n[2]+h]
            ACF = computeAutocorrelation(subset)
            corrLength = computeAutoCorrelationLength(ACF)
            # Save the results
            subsetResults[j,i] = corrLength
    if plot:
        # Plot the results
        fig, (ax1) = plt.subplots(nrows = 1, ncols = 1, figsize=(4,4))
        for i in range(subsetResults.shape[0]):
            ax1.plot(2*hws,subsetResults[i,:])
        ax1.set_xlim(2,maxHWZ*2)
        ax1.set_ylim(bottom=0)
        ax1.set_xlabel('Size [px]')
        ax1.set_ylabel('Autocorrelation length[px]')
        plt.tight_layout()
        plt.show()
    return 2*hws, subsetResults


def makeGrid(gridSize, spacing):
    """
    Creates a regular grid. The coordinates of the nodes are at the center of each element.

    Parameters
    -----------
        gridSize : list, int
            List with the dimensions of the grid
        
        spacing : scalar, int
            Value of the spacing between the nodes (size of the elements)

    Returns
    --------
        nodes : 2D array, int
            2D array with the coordinates (ZYX) of the center of the elements of the grid.
    """

    nodesGrid = numpy.mgrid[
        spacing//2 : gridSize[0] : spacing,
        spacing//2 : gridSize[1] : spacing,
        spacing//2 : gridSize[2] : spacing,
        ]
    numberOfNodes = int(nodesGrid.shape[1] * nodesGrid.shape[2] * nodesGrid.shape[3])

    nodes = numpy.zeros((numberOfNodes, 3))

    nodes[:, 0] = nodesGrid[0].ravel()
    nodes[:, 1] = nodesGrid[1].ravel()
    nodes[:, 2] = nodesGrid[2].ravel()

    return nodes.astype(int)

def localACF(image, nodes):
    """
    Creates a regular grid. The coordinates of the nodes are at the center of each element.

    Parameters
    -----------
        image : ND array
            A greyscale array
        
        nodes : 2D array, int
            Nodes of the 3D grid as given by fabricpy.autocorrelation.makeGrid()

    Returns
    --------
        anisotropy : 2D array, float
            1D array with the scalar anisotropy values of each element of the grid
        
        zinggCoord : 2D array, float
            2D array with the values [SM, ML] Zingg coordinates
        
        prolateVector : 2D array, float
            2D array with the orientation vector [ZYX] assuming that the local ACF is a prolate (rice)
        
        oblateVector : 2D array, float
            2D array with the orientation vector [ZYX] assuming that the local ACF is an oblate (lentil)

    """
    import spam.label
    isoLevel = 0.5

    # Get the spacing from the nodes
    spacing = nodes[0,0]
    # Get the number of nodes
    numNodes = len(nodes)
    # Create the result arrays
    anisotropy = numpy.zeros((numNodes))
    zinggCoord = numpy.zeros((numNodes,2))
    prolateVector = numpy.zeros((numNodes,3))
    oblateVector = numpy.zeros((numNodes,3))

    # Start of the function
    for i,n in enumerate(nodes):
        # Extract the element
        element = image[n[0]-spacing : n[0]+spacing,
                        n[1]-spacing : n[1]+spacing,
                        n[2]-spacing : n[2]+spacing]
        # Compute the ACF of the element
        ACF = computeAutocorrelation(element)
        # Compute the ellipsoid
        ellipsoid = generateEllipsoidFromIsosurface(ACF, isoLevel)
        # Compute its anisotropy
        anisotropyElement = fabricpy.anisotropy.computeScalarAnisotropy(ellipsoid)
        # Zingg coordinates
        SM, ML  = fabricpy.anisotropy.computeZinggCoordinates(ellipsoid)

        # Compute the raw vectors
        _, vector = fabricpy.anisotropy.computeOrientation(ellipsoid, rawVector=1)
        # Save the results
        anisotropy[i] = anisotropyElement
        zinggCoord[i,0] = SM
        zinggCoord[i,1] = ML
        prolateVector[i,:] = vector[0]
        oblateVector[i,:] = vector[1]
    
    return anisotropy, zinggCoord, prolateVector, oblateVector
