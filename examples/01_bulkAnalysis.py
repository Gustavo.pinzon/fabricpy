# ====================== #
#    ACF Bulk Analysis   #
# ====================== #
'''
This is an example for running ACF analysis.
The objective is to measure preferential planes/directions for the whole image.
'''

# Load the requiered packages
import fabricpy.dataset
import fabricpy.autocorrelation
import fabricpy.plotting
import fabricpy.anisotropy
import matplotlib.pyplot as plt

# ------------- #
# Reading data
# ------------- #

# Now lets load the data
im = fabricpy.dataset.loadLentil()
# Lets look at it
fig, (ax1,ax2,ax3) = plt.subplots(nrows = 1, ncols = 3, figsize=(9,3))
ax1.set_title('X-Y plane')
ax1.imshow(im[im.shape[0]//2,:,:], cmap = 'Greys_r')
ax2.set_title('Z-X plane')
ax2.imshow(im[:,im.shape[0]//2,:], cmap = 'Greys_r')
ax3.set_title('Z-Y plane')
ax3.imshow(im[:,:,im.shape[0]//2], cmap = 'Greys_r')
ax1.axis('off')
ax2.axis('off')
ax3.axis('off')
plt.tight_layout()
plt.show()

# ------------- #
# Computing ACF
# ------------- #

# It seems that we have some particle organisation. Run the ACF

# Compute the ACF
ACF = fabricpy.autocorrelation.computeAutocorrelation(im)
# Lets look at the raw ACF
fig, (ax1,ax2,ax3) = plt.subplots(nrows = 1, ncols = 3, figsize=(9,3))
ax1.set_title('X-Y plane')
ax1.imshow(ACF[ACF.shape[0]//2,:,:], cmap = 'Greys_r', vmin = 0, vmax = 1)
ax2.set_title('Z-X plane')
ax2.imshow(ACF[:,ACF.shape[0]//2,:], cmap = 'Greys_r', vmin = 0, vmax = 1)
ax3.set_title('Z-Y plane')
ax3.imshow(ACF[:,:,ACF.shape[0]//2], cmap = 'Greys_r', vmin = 0, vmax = 1)
ax1.axis('off')
ax2.axis('off')
ax3.axis('off')
plt.tight_layout()
plt.show()
# It is not symmetric! Lets plot it in 3D
fabricpy.plotting.plotACF(ACF, fileName=None, show = True)
# It is clear that there is a plane of symmetry of the ACF - this corresponds to the orientation of the particles!
# You can save the previous 3D plot by chanign `fileName` by the appropiate path+name+extension (e.g., './bulkACF.png')

# ------------------ #
# Measuring from ACF
# ------------------ #

# Lets measure some parameters from the ACF.

# We'll use the ellipsoid at a level of 0.5
level = 0.5
ellipsoid = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, level)
print('\n')
print('# --------------------------------------- #')
print('  Measurements at an ACF level of: ', level)
print('# --------------------------------------- #')
# Compute its anisotropy
anisotropy = fabricpy.anisotropy.computeScalarAnisotropy(ellipsoid)
print('\n')
print(' Anisotropy: ', anisotropy)
# Lets look at its Zingg coordinates
SM, ML  = fabricpy.anisotropy.computeZinggCoordinates(ellipsoid)
print('\n')
print(' S/M ratio: ', SM)
print(' M/L ratio: ', ML)
# Based on this results, we can classify it being full obalte (i.e., lentil-like),full prolate (i.e., rice-like), sphere-like or blade-like
# Oblates have a plane of symmetry, while prolates have a direction of symmetry.
# Sphere-like means that all the dimensions are mostly alike
# Blade-like means that all the dimension are different between them.
print('\n')
print(' Is like a lentil (Oblate)?: ', SM < 2/3 and ML > 2/3 )
print(' Is like a rice (Prolate)?:  ', SM > 2/3 and ML < 2/3 )
print(' Is like a Sphere?:          ', SM > 2/3 and ML > 2/3)
print(' Is like a Blade?:           ', SM < 2/3 and ML < 2/3)
# It is being classified as an sphere, but we can see that the SM ratio is very close to the 2/3 threshold.
# Lets try to measure its orientation
print('\n')
inclination, vector = fabricpy.anisotropy.computeOrientation(ellipsoid)
print(' Inclination: ', inclination,'[DEG]')
print(' Characteristic vector: ', vector,'[ZYX]')
# Here is shows that the inclination is NaN and its characteristic vector is [0,0,0]. 
#Since the shape is classified as a sphere-like, we can't objectively define a characteristic orientation.
# Lets retrieve the characteristic vector if we assume that it is a lentil or a rice.
inclination, vector = fabricpy.anisotropy.computeOrientation(ellipsoid, rawVector=1)
print('\n')
print(' Inclination Raw (rice):   ', inclination[0],'[DEG]')
print(' Inclination Raw (lentil): ', inclination[1],'[DEG]')
print(' Characteristic vector (rice): ', vector[0], '[ZYX]')
print(' Characteristic vector (lentil): ', vector[1], '[ZYX]')
print('\n')


