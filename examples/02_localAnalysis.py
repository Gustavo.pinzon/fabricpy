# ====================== #
#    ACF Local Analysis   #
# ====================== #
'''
This is an example for running ACF analysis at a local scale (i.e., running ACF for small subsets of the image)
The objective is to measure preferential planes/directions for the whole image.
This example is a continuation of the previous example '00_bulkAnalysis.py'.
'''

# Load the modules
import fabricpy.autocorrelation
import matplotlib.pyplot as plt
import fabricpy.dataset
# ------------- #
# Reading data
# ------------- #

# Lets load the data
im = fabricpy.dataset.loadLentil()

# Lets look at it
fig, (ax1,ax2,ax3) = plt.subplots(nrows = 1, ncols = 3, figsize=(9,3))
ax1.set_title('X-Y plane')
ax1.imshow(im[im.shape[0]//2,:,:], cmap = 'Greys_r')
ax2.set_title('Z-X plane')
ax2.imshow(im[:,im.shape[0]//2,:], cmap = 'Greys_r')
ax3.set_title('Z-Y plane')
ax3.imshow(im[:,:,im.shape[0]//2], cmap = 'Greys_r')
ax1.axis('off')
ax2.axis('off')
ax3.axis('off')
plt.tight_layout()
plt.show()

# --------------------- #
# Autocorrelation length
# --------------------- #

# The first question we need to ask is how big, or small, will our subsets be.
# One typical way to do it is to compute the distance at which the ACF decreases to 0.5.

# A first approach is to compute it for the whole image
# Compute the ACF
ACF = fabricpy.autocorrelation.computeAutocorrelation(im)#[100:, 100:, 100:])
# Compute the autocorrelation length
corrLength = fabricpy.autocorrelation.computeAutoCorrelationLength(ACF)
print('Autocorrelation length: ', corrLength, '[px]')
# This could be the minimum size of our elements for the local analysis.
# However, comparing it to the size of the particles we quickly see that this window size is too small for our local analysis.
# This is simply because we can't compute the autocorrelation length at the image scale.

# A better way to compute it, is to grab smaller subsets along the image and compute the autocorrelation length as a function of the size of the subsets
# For this we can use the computeAutoCorrelationLengthSubsets() function, using a node spacing of 50.
# The value of the node spacing affects the number of nodes, but also the maximum size of the windows.
# A lower node spacing yields a higher number of nodes. But if we don't reach convergence, we need to increase the spacing.
windowSize, subsetResults = fabricpy.autocorrelation.computeAutoCorrelationLengthSubsets(im, 50, plot=True)
# We see that at local subset size of 30 we already have convergence of the subsets.

# ------------- #
# Local analysis
# ------------- #

# First, we want to create some nodes with a given spacing: we will use 30px.
nodes = fabricpy.autocorrelation.makeGrid(im.shape,30)
# Now we feed the image and the grid to the ACF local function to run the ACF analysis for each of the elements.
anisotropy, zinggCoord, prolateVector, oblateVector = fabricpy.autocorrelation.localACF(im, nodes)

# ------------- #
# Results
# ------------- #

# First lets look at an histogram of the anisotropy
fig, (ax1) = plt.subplots(nrows = 1, ncols = 1, figsize=(5,3))
ax1.hist(anisotropy, bins=75)
ax1.set_xlim(0,1)
ax1.set_xlabel('Scalar anisotropy')
ax1.set_ylabel('Counts')
plt.tight_layout()
plt.show()

# We see that the distribution of the scalar anisotropy is centered around 0.38, which suggest that we have some sort of preferential orientation/plane of our system.

# Now lets generate the Zingg plot
# The x axis correspond to the ratio between the small and medium axes, and the y axis correspond to the ratio between the medium and large axes.
# This means that the upper left corner corresponds to oblate (lentil) shapes, while the lower right corner correspond to prolate (rice) shapes.
fig, (ax1) = plt.subplots(nrows = 1, ncols = 1, figsize=(4,4))
ax1.scatter(zinggCoord[:,0], zinggCoord[:,1], s = 8, c = 'b', alpha = 0.5)
ax1.plot([0,1],[2/3,2/3], '--k')
ax1.plot([2/3,2/3], [0,1], '--k')
ax1.set_xlim(0,1)
ax1.set_ylim(0,1)
ax1.set_xlabel('S/M')
ax1.set_ylabel('M/L')
ax1.set_aspect('equal')
plt.tight_layout()
plt.show()

# It is clear that most of our subsets fall within the upper right corner (sphere-like).
# However, they are not fully isotropic, the histogram of the scalar anisotropy shows some peaks around 0.35.
# This is clear when seeing that the distribution is centered near the oblate-sphere limit, which means that we can use the oblate vector to further analyse our dataset.

# We can compute the histogram of the Zingg coordinates, for an easy visualisation of the previous statements.

fig, (ax1) = plt.subplots(nrows = 1, ncols = 1, figsize=(5,3))
ax1.hist(zinggCoord[:,0], bins=75, color='b', label='S/M', alpha = 0.5)
ax1.hist(zinggCoord[:,1], bins=75, color='r', label='M/L', alpha = 0.5)
_, ymax = ax1.get_ylim()
ax1.plot([2/3,2/3],[0, ymax], '--k')
ax1.set_xlim(0,1)
ax1.set_ylim(0,ymax)
ax1.set_xlabel('Index (S/M or M/L)')
ax1.set_ylabel('Counts')
plt.legend()
plt.tight_layout()
plt.show()

# The dashed line marks the limit of 2/3 between the sphere-like sector and the anisotropic regions (either prolate or oblate).
# Here is clear that the distribution of 'S/M' is closer to the 2/3 limits, which suggest that we might use the oblate characteristic orientation vector.
# In contrast, the distribution of 'M/L' is centered within the sphere-like region (M/L > 2/3). 
