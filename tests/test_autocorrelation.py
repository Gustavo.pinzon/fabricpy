# -*- coding: utf-8 -*-

import unittest
import numpy
import scipy.ndimage

import fabricpy.autocorrelation

class TestClass(unittest.TestCase):

    def test_imageNormalisation(self):
        # Create a random 3D array
        im = numpy.random.rand(5,5,5)
        # Normalise it
        imNorm = fabricpy.autocorrelation.normaliseVolume(im)
        # Check its mean and variance
        self.assertAlmostEqual(numpy.mean(imNorm), 0, places = 5)
        self.assertAlmostEqual(numpy.std(imNorm), 1, places = 5)

    def test_autocorrelation(self):
        # Create an empty array
        im = numpy.zeros((5,5,5))
        imZ = im.copy()
        imY = im.copy()
        imX = im.copy()
        # Fill with a line along each axes
        imZ[:,2,2] = 1
        imY[2,:,2] = 1
        imX[2,2,:] = 1
        # Compute the ACF
        ACFZ = fabricpy.autocorrelation.computeAutocorrelation(imZ)
        ACFY = fabricpy.autocorrelation.computeAutocorrelation(imY)
        ACFX = fabricpy.autocorrelation.computeAutocorrelation(imX)
        # Check that along each axes there is only 1
        self.assertAlmostEqual(numpy.sum(imZ[:,2,2]/5), 1, places = 5)
        self.assertAlmostEqual(numpy.sum(imY[2,:,2]/5), 1, places = 5)
        self.assertAlmostEqual(numpy.sum(imX[2,2,:]/5), 1, places = 5)


    def test_computeIsoSurface(self):
        # Create a sphere and add blur!
        im = numpy.zeros((100,100,100))
        # Set the radius of the sphere
        radii = 30
        # Create the sphere
        for z in range(0,100):
            for y in range(0,100):
                for x in range(0,100):
                    # Compute the distance to the center
                    dist = numpy.sqrt((z-50)**2 + (y-50)**2 + (x-50)**2)
                    if dist <= radii:
                        im[z,y,x] = 1
        # Apply some blur
        im = scipy.ndimage.gaussian_filter(im, sigma=0.8)
        # Compute the ACF
        ACF = fabricpy.autocorrelation.computeAutocorrelation(im)
        # Compute the ellipsoids
        ellip02 = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, 0.2)
        ellip05 = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, 0.5)
        ellip08 = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, 0.8)
        # Check that all are valid
        self.assertTrue(numpy.sum(ellip02) > 0)
        self.assertTrue(numpy.sum(ellip05) > 0)
        self.assertTrue(numpy.sum(ellip08) > 0)
        # Check that they are in order
        self.assertTrue(numpy.sum(ellip02) > numpy.sum(ellip05))
        self.assertTrue(numpy.sum(ellip02) > numpy.sum(ellip08))
        self.assertTrue(numpy.sum(ellip05) > numpy.sum(ellip08))

        # Compute the isoSurface
        iso02 = fabricpy.autocorrelation.computeIsoSurface(ACF, 0.2)
        iso05 = fabricpy.autocorrelation.computeIsoSurface(ACF, 0.5)
        iso08 = fabricpy.autocorrelation.computeIsoSurface(ACF, 0.8)
        # Check that all the convex hull are valid
        self.assertTrue(iso02.volume > 0)
        self.assertTrue(iso05.volume > 0)
        self.assertTrue(iso08.volume > 0)
        # 2. Check that they are in order
        self.assertTrue(iso02.volume > iso05.volume)
        self.assertTrue(iso02.volume > iso08.volume)
        self.assertTrue(iso05.volume > iso08.volume)

    def test_computeAutoCorrelationLength(self):
        # Create a sphere and add blur!
        im = numpy.zeros((100,100,100))
        # Set the radius of the sphere
        radii = 40
        # Create the sphere
        for z in range(0,100):
            for y in range(0,100):
                for x in range(0,100):
                    # Compute the distance to the center
                    dist = numpy.sqrt((z-50)**2 + (y-50)**2 + (x-50)**2)
                    if dist <= radii:
                        im[z,y,x] = 1
        # Apply some blur
        im = scipy.ndimage.gaussian_filter(im, sigma=0.8)
        # Compute the ACF
        ACF = fabricpy.autocorrelation.computeAutocorrelation(im)
        corrLength = fabricpy.autocorrelation.computeAutoCorrelationLength(ACF)
        # Basic test to check that it runs
        self.assertTrue(corrLength > 0)
        self.assertTrue(corrLength < radii)


if __name__ == '__main__':
        unittest.main()
