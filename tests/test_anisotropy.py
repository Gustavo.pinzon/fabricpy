# -*- coding: utf-8 -*-

import unittest
import numpy
import scipy.ndimage

import fabricpy.autocorrelation
import fabricpy.anisotropy

class TestClass(unittest.TestCase):

    def test_MOI(self):
        # Create a plate and add blur!
        plate = numpy.zeros((100,100,100))
        plate[40:60,20:80,20:80] = 1
        rod = numpy.zeros((100,100,100))
        rod[20:80,40:60,40:60] = 1

        # 1. Compute the MOI of a plate
        im = plate.copy()
        # Apply some blur
        im = scipy.ndimage.gaussian_filter(im, sigma=0.8)
        # Compute the ACF
        ACF = fabricpy.autocorrelation.computeAutocorrelation(im)
        # Compute the ellipsoid
        im = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, 0.5)
        # Compute the MOI eigval and eigvect
        eigVals, eigVect = fabricpy.anisotropy.computeMomentOfInertia(im)
        # The plate is symmetric, so check that e1 > e2 = e3.
        self.assertTrue(eigVals[0] > eigVals[1])
        self.assertTrue(eigVals[0] > eigVals[2])
        self.assertAlmostEqual(eigVals[1], eigVals[2], places = 5)
        # Check the orientation of the plate
        inclination = numpy.degrees(numpy.arcsin(eigVect[0:3][0]))
        self.assertAlmostEqual(inclination, 90, places = 5)

        # 1. Compute the MOI of a rod
        im = rod.copy()
        # Apply some blur
        im = scipy.ndimage.gaussian_filter(im, sigma=0.8)
        # Compute the ACF
        ACF = fabricpy.autocorrelation.computeAutocorrelation(im)
        # Compute the ellipsoid
        im = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, 0.5)
        # Compute the MOI eigval and eigvect
        eigVals, eigVect = fabricpy.anisotropy.computeMomentOfInertia(im)
        # The rod is symmetric, so check that e1 = e2 > e3.
        self.assertAlmostEqual(eigVals[0], eigVals[1], places = 5)
        self.assertTrue(eigVals[0] > eigVals[2])
        self.assertTrue(eigVals[1] > eigVals[2])
        # # Check the orientation of the rod
        inclination = numpy.degrees(numpy.arcsin(eigVect[6:9][0]))
        self.assertAlmostEqual(inclination, 90, places = 5)

    def test_semiAxes(self):
        # 1. Test a sphere
        # Create a sphere and add blur!
        im = numpy.zeros((100,100,100))
        # Set the radius of the sphere
        radii = 30
        # Create the sphere
        for z in range(0,100):
            for y in range(0,100):
                for x in range(0,100):
                    # Compute the distance to the center
                    dist = numpy.sqrt((z-50)**2 + (y-50)**2 + (x-50)**2)
                    if dist <= radii:
                        im[z,y,x] = 1
        L,M,S = fabricpy.anisotropy.computeSemiAxesEllipsoid(im)
        # Check that all the values are the same
        self.assertAlmostEqual(L,M, places = 5)
        self.assertAlmostEqual(L,S, places = 5)
        self.assertAlmostEqual(S,M, places = 5)

        # 2. Test Lentils and Rice - use the data from the previous test
        # Create a plate and add blur!
        plate = numpy.zeros((100,100,100))
        plate[40:60,20:80,20:80] = 1
        rod = numpy.zeros((100,100,100))
        rod[20:80,40:60,40:60] = 1

        # 2.1. Use a plate
        im = plate.copy()
        # Apply some blur
        im = scipy.ndimage.gaussian_filter(im, sigma=0.8)
        # Compute the ACF
        ACF = fabricpy.autocorrelation.computeAutocorrelation(im)
        # Compute the ellipsoid
        im = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, 0.5)
        # Compute the semi-axes
        L,M,S = fabricpy.anisotropy.computeSemiAxesEllipsoid(im)
        # The plate is symmetric, so check that L = M > S
        self.assertAlmostEqual(M, L, places = 5)
        self.assertTrue(M > S)
        self.assertTrue(L > S)
        
        # 2.2. Use a rod
        im = rod.copy()
        # Apply some blur
        im = scipy.ndimage.gaussian_filter(im, sigma=0.8)
        # Compute the ACF
        ACF = fabricpy.autocorrelation.computeAutocorrelation(im)
        # Compute the ellipsoid
        im = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, 0.5)
        # Compute the semi-axes
        L,M,S = fabricpy.anisotropy.computeSemiAxesEllipsoid(im)
        # The rice is symmetric, so check that L > M = S
        self.assertAlmostEqual(M, S, places = 5)
        self.assertTrue(L > S)
        self.assertTrue(L > M)
    
    def test_ZinggCoordinates(self):
        
        # 1. Test a sphere
        sphere = numpy.zeros((100,100,100))
        # Set the radius of the sphere
        radii = 30
        # Create the sphere
        for z in range(0,100):
            for y in range(0,100):
                for x in range(0,100):
                    # Compute the distance to the center
                    dist = numpy.sqrt((z-50)**2 + (y-50)**2 + (x-50)**2)
                    if dist <= radii:
                        sphere[z,y,x] = 1
        # Compute the Zingg coordinates
        SM, ML = fabricpy.anisotropy.computeZinggCoordinates(sphere)
        # Check that both are the same
        self.assertAlmostEqual(SM, ML, places = 5)
        # Check that they are within the sphere quadrant
        self.assertTrue(SM > 2/3)
        self.assertTrue(ML > 2/3)

        # 2. Test a lentil
        plate = numpy.zeros((100,100,100))
        plate[40:60,20:80,20:80] = 1
        # Apply some blur
        plate = scipy.ndimage.gaussian_filter(plate, sigma=0.8)
        # Compute the ACF
        ACF = fabricpy.autocorrelation.computeAutocorrelation(plate)
        # Compute the ellipsoid
        lentil = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, 0.5)
        # Compute the Zingg coordinates
        SM, ML = fabricpy.anisotropy.computeZinggCoordinates(lentil)
        # Check that they are within the lentil quadrant
        self.assertTrue(SM < 2/3)
        self.assertTrue(ML > 2/3)

        # 3. Test a rice
        rod = numpy.zeros((100,100,100))
        rod[20:80,40:60,40:60] = 1
        # Apply some blur
        rod = scipy.ndimage.gaussian_filter(rod, sigma=0.8)
        # Compute the ACF
        ACF = fabricpy.autocorrelation.computeAutocorrelation(rod)
        # Compute the ellipsoid
        rice = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, 0.5)
        # Compute the Zingg coordinates
        SM, ML = fabricpy.anisotropy.computeZinggCoordinates(rice)
        # Check that they are within the rice quadrant
        self.assertTrue(SM > 2/3)
        self.assertTrue(ML < 2/3)
        
    def test_scalarAnisotropy(self):

        # 1. Test a sphere
        sphere = numpy.zeros((100,100,100))
        # Set the radius of the sphere
        radii = 30
        # Create the sphere
        for z in range(0,100):
            for y in range(0,100):
                for x in range(0,100):
                    # Compute the distance to the center
                    dist = numpy.sqrt((z-50)**2 + (y-50)**2 + (x-50)**2)
                    if dist <= radii:
                        sphere[z,y,x] = 1
        # Compute its anisotropy
        zeta = fabricpy.anisotropy.computeScalarAnisotropy(sphere)
        self.assertAlmostEqual(zeta, 0, places = 5)

        # 2. Test a line
        line = numpy.zeros((100,100,100))
        line[:,50,50] = 1
        zeta = fabricpy.anisotropy.computeScalarAnisotropy(line)
        self.assertTrue(zeta > 0.9)

    def test_orientation(self):
        # Create a sphere
        sphere = numpy.zeros((100,100,100))
        # Set the radius of the sphere
        radii = 30
        # Create the sphere
        for z in range(0,100):
            for y in range(0,100):
                for x in range(0,100):
                    # Compute the distance to the center
                    dist = numpy.sqrt((z-50)**2 + (y-50)**2 + (x-50)**2)
                    if dist <= radii:
                        sphere[z,y,x] = 1

        # Create a plate and add blur!
        plate = numpy.zeros((100,100,100))
        plate[40:60,20:80,20:80] = 1

        # 1. Use a sphere
        inclination, vector = fabricpy.anisotropy.computeOrientation(sphere)
        # Check that inclination is nan and vector is [0,0,0]
        self.assertTrue(numpy.isnan(inclination))
        self.assertTrue(numpy.linalg.norm(vector) == 0)

        # 2. Use a plane randomly oriented
        # Apply some blur
        im = scipy.ndimage.gaussian_filter(plate, sigma=0.8)
        # Rotate the image
        rotationAngle = 90*(2*numpy.random.rand(1)[0]-1)
        im = scipy.ndimage.rotate(im, rotationAngle, axes=(1, 0))
        # Compute the ACF
        ACF = fabricpy.autocorrelation.computeAutocorrelation(im)
        # Compute the ellipsoid
        ellipsoid = fabricpy.autocorrelation.generateEllipsoidFromIsosurface(ACF, 0.5)

        inclination, vector = fabricpy.anisotropy.computeOrientation(ellipsoid)
        expectedInclination = 90-numpy.abs(rotationAngle)
        self.assertAlmostEqual(inclination, expectedInclination, places = 0)        

if __name__ == '__main__':
        unittest.main()
